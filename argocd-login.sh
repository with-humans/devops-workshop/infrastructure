#!/bin/bash

set -euxo pipefail

argocd \
  login \
  argocd.k3d.local.with-humans.org \
  --username admin \
  --password password
