#!/bin/bash
set -e +x

CLUSTER_DIR=$(dirname "${BASH_SOURCE[0]}")
SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=0

command_exists() {
    local COMMAND="$1"
    local INSTALL_GUIDE="$2"
    if ! command -v "$COMMAND" >/dev/null; then
        echo "Install '${COMMAND}' ( see ${INSTALL_GUIDE} )"
        SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=1
    fi
}

has_iptables_input_chain_set_to_accept() {
    if ! command -v iptables || ! sudo iptables -L INPUT; then 
      echo "no iptables. skipping check";
      return 0;
    fi

    echo "The following sudo prompt is necessary to view your iptables configuration."
    set -x
    local INPUT_CHAIN=$(sudo iptables -L INPUT)
    if ! [[ "$INPUT_CHAIN" =~ "policy ACCEPT" ]]; then
        echo "Your iptables are configured to reject any unmatched package on INPUT"
        echo "For other users, this has lead to the host ports not being reachable from a container"
        echo "This script will run another more elaborate script that will test whether this is true for your system"
        echo "If true, running sudo iptables -P INPUT ACCEPT will fix this problem for your current session."
        echo ""
        set +e
        $(dirname $0)/test-host-port-is-reachable-in-container.sh
        set -e

        if [ "$?" -ne 0 ]; then
            SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=1
        fi
    fi
    set +x
}

has_sufficient_inotify_instances() {
    # mac os doesn't have fs.inotify, but we also haven't had issues
    if [[ $(uname) = "Darwin" ]]; then return 0; fi
    if [[ $(sysctl fs.inotify.max_user_instances -n) -le 1023 ]]; then
        echo "Your fs.inotify.max_user_instances limit is too low for loki and other file-heavy operations"
        echo "Please increase it to at least 1024 using: "
        echo 'echo "fs.inotify.max_user_instances=1024" | sudo tee /etc/sysctl.d/99-inotify.conf; sudo sysctl --system'
        SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=1
    fi
}

try_ping_host() {
    local HOST="$1"
    ping -c1 -q "$HOST" -W1 >/dev/null
    local RETVAL=$?
    if [ "$RETVAL" -ne 0 ]; then
        echo "🟥 $HOST"
        return $RETVAL
    else
        echo "✅ $HOST"
        return 0
    fi
}

can_resolve_local_domain() {
    local DOMAIN_SUFFIX="local.with-humans.org"
    set +e
    try_ping_host "hello.k3d.$DOMAIN_SUFFIX"
    try_ping_host "random_name.$DOMAIN_SUFFIX"
    local RETVAL=$?
    set -e

    if [ "$RETVAL" -ne 0 ]; then
        echo "Could not resolve some domains to 127.0.0.1. Your router might be preventing DNS-rebinds."
        echo "You have the following options:"
        echo " - Specify a DNS server that resolves hosts to 127.0.0.1, like 1.1.1.1, 8.8.8.8 or 8.8.4.4"
        echo " - Enable DNSMasq (NetworkManager/Linux) and add a manual entry for those domains (see https://gist.github.com/ju1ius/944ad78f4db9188cd3cafc171aab8b98)"
        echo ""
        echo "If you are using systemd-resolved (check via resolvectl query www.google.com), you may be able to create a file /etc/systemd/resolved.conf.d/local.with-humans.org.conf with the content"
        echo "[Resolve]"
        echo "DNS=8.8.8.8"
        echo "Domains=~local.with-humans.org"

        SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=1
    fi
}

command_exists "docker" "https://docs.docker.com/"

if ! $CLUSTER_DIR/find-step-cli.sh >/dev/null; then
    echo "Please install step-cli (https://smallstep.com/docs/step-cli/installation)"
    SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=1
fi

if ! $CLUSTER_DIR/find-docker-compose.sh >/dev/null; then
    echo "Please install docker-compose (https://docs.docker.com/compose/)"
    SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=1
fi

if ! $CLUSTER_DIR/test-host-port-is-reachable-in-container.sh; then
    echo "Can't reach host port from container"
    SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS=1
fi

can_resolve_local_domain
has_iptables_input_chain_set_to_accept
has_sufficient_inotify_instances

if [ "${SHOULD_EXIT_AFTER_RUNNING_OTHER_TESTS}" -ne 0 ]; then
    echo ""
    echo "You need to install the dependencies listed above to continue."
    exit 1
fi
