#!/bin/bash

set -e

CLIENT_NETWORK=
SERVER_NETWORK=
SERVER_CONTAINER=

create_networks() {
    SERVER_NETWORK=$(docker network create $(openssl rand -hex 6))
    CLIENT_NETWORK=$(docker network create $(openssl rand -hex 6))
}

start_server_container() {
    SERVER_CONTAINER=$(docker run --network "$SERVER_NETWORK" -d -p 8080 traefik/whoami:v1.10.0)
    SERVER_PORT=$(docker inspect --format '{{ index .NetworkSettings.Ports "8080/tcp" 0 "HostPort" }}' "$SERVER_CONTAINER")
}

try_connecting_to_host_port() {
    set +e
    docker run --network "$CLIENT_NETWORK" --add-host=host.docker.internal:host-gateway --rm curlimages/curl curl -v "http://host.docker.internal:${SERVER_PORT}"
    local RETVAL=$?
    set -e
    if [ "$RETVAL" -ne 0 ]; then
        echo "Error reaching host port using another docker container" >/dev/stderr
    fi
    return "$RETVAL"
}

clean_up() {
    set +e
    docker rm -f "$SERVER_CONTAINER"
    docker network rm "$SERVER_NETWORK"
    docker network rm "$CLIENT_NETWORK"
}

run() {
    create_networks
    start_server_container

    try_connecting_to_host_port
}

trap clean_up EXIT

run
