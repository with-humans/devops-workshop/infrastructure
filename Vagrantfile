# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = ENV['VAGRANT_BOX'] || "ubuntu/jammy64"



  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  config.vm.network "forwarded_port", guest: 80, host: 80
  config.vm.network "forwarded_port", guest: 443, host: 443
  config.vm.network "forwarded_port", guest: 5003, host: 5003
  unless ENV['VAGRANT_USE_HOST_REGISTRIES']
    config.vm.network "forwarded_port", guest: 5001, host: 5001
    config.vm.network "forwarded_port", guest: 5002, host: 5002
    config.vm.network "forwarded_port", guest: 5004, host: 5004
  end
  
  config.trigger.before :up do |trigger|
    trigger.info = "Checking ingress certificate"
    trigger.run = { inline: <<-SHELL
      bash -c '
        set -ex

        source "./_config.sh"
        STEP_CLI=$(./find-step-cli.sh)
        mkdir -p ${LOCAL_CLUSTER_DIR}/output/certs || true

        if ! { [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" ] && [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_KEY_FILE}" ] && [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}" ]; } ||
          ! ${STEP_CLI} certificate verify --roots "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"; then
          ./generate-ingress-certificates.sh "k3d.local.with-humans.org" "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_KEY_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"
        fi

        if ! ${STEP_CLI} certificate verify "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"; then
          echo "You will be prompted for your sudo password so step-cli can install the certificate in your CA trust stores"
          ${STEP_CLI} certificate install "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}"
        fi
        '
    SHELL
    }
  end

  config.trigger.before :up do |trigger|
    trigger.info = "Checking Port Availability"
    trigger.run = { inline: <<-SHELL
      bash -c '
        if command -v sysctl;
        then
          if [ `sysctl -n net.ipv4.ip_unprivileged_port_start` -gt 80 ];
          then
            echo "Your system settings prevent vagrant from using port 80 and 443. This is a good security feature, but prevents us from running the cluster properly."
            echo "We suggest you run sysctl net.ipv4.ip_unprivileged_port_start=80 for this session (do not persist it) to enable vagrant to use these ports."
            exit 1
          fi
        fi
      '
    SHELL
    }
  end

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.cpus = 6
    vb.memory = ENV['RAM'] || 14*1024
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.

  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    sudo resolvectl dns enp0s3 8.8.8.8
  SHELL

  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    echo "fs.inotify.max_user_instances=1024" | sudo tee /etc/sysctl.d/99-inotify.conf
    sudo sysctl --system
  SHELL

  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    set -euo pipefail
    sudo apt-get update

    sudo apt-get install -y \
      ca-certificates \
      curl \
      jq \
      gnupg \
      lsb-release

    sudo mkdir -p /etc/apt/keyrings || true

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update
    
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin jq

    sudo usermod -aG docker vagrant
    newgrp docker
  SHELL

  if ENV["VAGRANT_USE_LAKETOWER_MIRROR"]
    config.vm.provision "shell", privileged: false, inline: <<-SHELL
      echo '{\"registry-mirrors\": [\"https://mirror.laketower.dyn.reemo.de\"]}' | sudo tee /etc/docker/daemon.json
      sudo systemctl restart docker
    SHELL
  else
    config.vm.provision "shell", privileged: false, inline: <<-SHELL
    echo '{\"registry-mirrors\": [\"https://mirror.gcr.io\"]}' | sudo tee /etc/docker/daemon.json
    sudo systemctl restart docker
    SHELL
  end


  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    set -x
    # renovate: datasource=github-releases depName=smallstep/cli versioning=semver
    export STEP_VERSION=v0.28.3
    wget -O- https://dl.step.sm/gh-release/cli/gh-release-header/${STEP_VERSION}/step_linux_${STEP_VERSION:1}_amd64.tar.gz | \
      sudo tar -xzf- --no-same-owner --strip-components=2 -C /usr/local/bin step_${STEP_VERSION:1}/bin/step && step --version
  SHELL
end
