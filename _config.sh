#!/bin/bash
set -euxo pipefail

export DOMAIN=k3d.local.with-humans.org
export LOCAL_CLUSTER_DIR=$(dirname "${BASH_SOURCE[0]}")
export HELM_TIMEOUT="30m"

export VAGRANT_USE_HOST_REGISTRIES="${VAGRANT_USE_HOST_REGISTRIES=}"

export USE_TOOLING_IN_DOCKER="${USE_TOOLING_IN_DOCKER=yes}"
export DOCKER_COMPOSE=$($LOCAL_CLUSTER_DIR/find-docker-compose.sh)
if [ "$USE_TOOLING_IN_DOCKER" == "yes" ]; then
    export TOOLING="${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/docker-compose.yaml" exec \
      -e INGRESS_CA_CRT_FILE \
      -e INGRESS_CRT_FILE \
      -e INGRESS_KEY_FILE \
      -e DOMAIN
      tooling"
    export CLUSTER_DIR="/infrastructure/"
else
    export TOOLING=""
    export CLUSTER_DIR="$LOCAL_CLUSTER_DIR"
fi

CERT_DIR="output/certs"
export INGRESS_CA_CRT_FILE="${CERT_DIR}/ingress-ca.crt"
export INGRESS_KEY_FILE="${CERT_DIR}/ingress.key"
export INGRESS_CRT_FILE="${CERT_DIR}/ingress.crt"
