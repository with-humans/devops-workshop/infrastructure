#!/bin/bash

set -ex
STEP_CLI=$(./find-step-cli.sh)

CLUSTER_DIR=${CLUSTER_DIR:=$(dirname "$0")}

CA_KEY_FILE=$(mktemp)
DOMAIN="$1"
CA_CRT_FILE="$2"
KEY_FILE="$3"
CRT_FILE="$4"

${STEP_CLI} certificate create --force --profile root-ca --no-password --insecure "K3D Root CA" "${CA_CRT_FILE}" "${CA_KEY_FILE}"

${STEP_CLI} certificate create localhost "${CRT_FILE}" "${KEY_FILE}" --profile leaf \
    --not-after=720h --no-password --insecure --force --ca "${CA_CRT_FILE}" --ca-key "${CA_KEY_FILE}" \
    --san host.k3d.internal --san ${DOMAIN} --san \*.${DOMAIN}

rm "$CA_KEY_FILE"
