#!/bin/bash

set -euxo pipefail

install_basics() {
    apt-get update
    apt-get install -y sudo curl gnupg iputils-ping
}

install_kubectl() {
    apt-get update && apt-get install -y apt-transport-https
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
    apt-get update
    apt-get install -y kubectl
}

install_jq() {
    apt-get install -y jq
}

install_docker() {
    apt-get update

    apt-get install -y \
      ca-certificates \
      curl \
      gnupg \
      lsb-release

    mkdir -p /etc/apt/keyrings || true

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt-get update
    
    apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
}

install_homebrew() {
    export NONINTERACTIVE=1
    export HOMEBREW_INSTALL_FROM_API=1
    export HOMEBREW_CC=gcc
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    apt-get install -y build-essential
    echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> $HOME/.profile
    echo 'export HOMEBREW_INSTALL_FROM_API=1' >> $HOME/.profile
    echo 'export HOMEBREW_CC=gcc' >> $HOME/.profile
    eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
}

install_homebrew_dependencies() {
    brew tap shellspec/shellspec
    brew install argo argocd shellspec yq k3d helm step linkerd logcli kubectl
    ln -s /home/linuxbrew/.linuxbrew/bin/step /home/linuxbrew/.linuxbrew/bin/step-cli
}

install_basics
install_kubectl
install_jq
install_docker
install_homebrew
install_homebrew_dependencies