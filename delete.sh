#!/bin/bash
set -euxo pipefail
source "$(dirname "${BASH_SOURCE[0]}")/_config.sh"
${DOCKER_COMPOSE} exec -it tooling k3d cluster delete
${TOOLING} bash -c 'chown '"$(id -u):$(id -g)"' -R $HOME/.kube/'
