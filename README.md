| # | DevOps | Workshop - Infrastructure |

This repository contains all necessary resources to bootstrap a self-contained kubernetes cluster for the workshop.

## Getting Started

### Setup Cluster

The install script will check if you have all necessary prerequisites installed.

```sh
./create.sh
```

**Your setup is only completed if this script executes the test suite successfully!**

### Upgrade cluster

```sh
./upgrade.sh
```

### Verify cluster state

If you run into problems at any time, run the tests to check for known issues:

```sh
./verify.sh
```

## Infrastructure Services URLs

Go to [the workshop site](https://welcome.k3d.local.with-humans.org) for available services.

## Alerting

To enable pushover notifications:

```
kubectl create secret generic pushover --from-literal=token=$PUSHOVER_TOKEN --from-literal=userkey=$PUSHOVER_USERKEY
kubectl apply -f alertmanager-pushover.yaml
```
