#!/bin/bash

set -euxo pipefail

source "$(dirname "${BASH_SOURCE[0]}")/_config.sh"

getAgent() {
    k3d node list --output=json | jq -r 'map(select(.role == "agent")) | first | .name'
}

run() {
    agentName=$(getAgent)

    docker exec "${agentName}" crictl --debug pull alpine:latest
    docker exec "${agentName}" ctr --debug image pull --plain-http host.k3d.internal:5001/library/alpine:latest
}

run
