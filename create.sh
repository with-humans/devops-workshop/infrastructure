#!/bin/bash

set -euxo pipefail

source "$(dirname "${BASH_SOURCE[0]}")/_config.sh"
mkdir -p ${LOCAL_CLUSTER_DIR}/output/certs || true

"${LOCAL_CLUSTER_DIR}/prerequisites.sh"


STEP_CLI=$($LOCAL_CLUSTER_DIR/find-step-cli.sh)
if [ ! "$STEP_CLI" == "step" ]; then
  step() {
    "$STEP_CLI" "$@"
  }
fi

set +u
if ! [ -z "$TOOLING_IMAGE_TAG" ] && \
  ! docker pull registry.gitlab.com/with-humans/devops-workshop/infrastructure/tooling:${TOOLING_IMAGE_TAG}
then
  echo "No image for tooling tag $TOOLING_IMAGE_TAG, using latest"
  TOOLING_IMAGE_TAG=latest
fi
set -u  

${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/docker-compose.yaml" pull
# make sure registries are up
if [[ -n "$VAGRANT_USE_HOST_REGISTRIES" ]]; then
  ${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/local-pullthrough-registries.vagrant.docker-compose.yaml" up -d
  if [ "$USE_TOOLING_IN_DOCKER" == "yes" ]; then
    ${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/docker-compose.yaml" up tooling -d
  fi
else
  ${DOCKER_COMPOSE} -f "${LOCAL_CLUSTER_DIR}/docker-compose.yaml" up -d
fi

# Check ingress certificate
if ! { [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" ] && [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_KEY_FILE}" ] && [ -f "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}" ]; } ||
  ! ${STEP_CLI} certificate verify --roots "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"; then
  echo "Generating new ingress certificate and installing the root CA"
  ${LOCAL_CLUSTER_DIR}/generate-ingress-certificates.sh "${DOMAIN}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_KEY_FILE}" "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"
else
  echo "Using existing ingress certificate"
fi

if ! ${STEP_CLI} certificate verify "${LOCAL_CLUSTER_DIR}/${INGRESS_CRT_FILE}"; then
  echo "You will be prompted for your sudo password so step-cli can install the certificate in your CA trust stores"
  ${STEP_CLI} certificate install --all "${LOCAL_CLUSTER_DIR}/${INGRESS_CA_CRT_FILE}"
fi
${TOOLING} step certificate install --all "$INGRESS_CA_CRT_FILE"




# renovate: datasource=docker depName=rancher/k3s versioning=regex:^v(?<major>\d+)\.(?<minor>\d+)\.(?<patch>\d+)-k3s(?<build>\d+)$
K3S_VERSION=v1.32.1-k3s1


# create cluster
${TOOLING} k3d cluster create \
  --k3s-arg "--disable=traefik@server:*" \
  --image "docker.io/rancher/k3s:${K3S_VERSION}" \
  --agents 2 \
  -p "80:80@loadbalancer" \
  -p "443:443@loadbalancer" \
  --registry-config "$CLUSTER_DIR/local-pullthrough-registries.k3d-registries.yaml" \
  --registry-create registry:0.0.0.0:5003 \
  --api-port 127.0.0.1:42839

# Make sure user owns kube-config
${TOOLING} bash -c 'chown '"$(id -u):$(id -g)"' -R $HOME/.kube/'

${TOOLING} kubectl wait --timeout 5m --for=condition=Available deployment metrics-server -n kube-system

# create DNS rewrite so *.k3d.local.with-humans.org resolves to host.k3d.internal
${TOOLING} kubectl apply -f "${CLUSTER_DIR}/ingress-dns-entry.yaml"
# Restart coredns because reload isn't reliable
${TOOLING} kubectl rollout restart -n kube-system deployment.apps/coredns
${TOOLING} kubectl rollout status -n kube-system deployment.apps/coredns

until ${TOOLING} ${CLUSTER_DIR}/validate-registries-are-reachable.sh; do
  sleep 1
done


# install blackbox-exporter
${TOOLING} kubectl create namespace observability
${TOOLING} kubectl create configmap \
  -n observability \
  certificate-host.k3d.internal \
  --from-file "host.k3d.internal.crt=${CLUSTER_DIR}/${INGRESS_CRT_FILE}"


# renovate: datasource=github-releases depName=tektoncd/pipeline versioning=semver
TEKTON_PIPELINE_VERSION=0.68.0
${TOOLING} kubectl apply --filename \
  https://storage.googleapis.com/tekton-releases/pipeline/previous/v${TEKTON_PIPELINE_VERSION}/release.yaml

${TOOLING} kubectl wait --timeout 5m --for=condition=Available --namespace tekton-pipelines deployment/tekton-pipelines-controller
${TOOLING} kubectl wait --timeout 5m --for=condition=Available --namespace tekton-pipelines deployment/tekton-pipelines-webhook

# renovate: datasource=github-releases depName=tektoncd/triggers versioning=semver
TEKTON_TRIGGERS_VERSION=0.31.0
${TOOLING} kubectl apply --filename \
  https://storage.googleapis.com/tekton-releases/triggers/previous/v${TEKTON_TRIGGERS_VERSION}/release.yaml

${TOOLING} kubectl apply --filename \
  https://storage.googleapis.com/tekton-releases/triggers/previous/v${TEKTON_TRIGGERS_VERSION}/interceptors.yaml



${LOCAL_CLUSTER_DIR}/helmfile.sh sync --wait
${LOCAL_CLUSTER_DIR}/helmfile.sh test

# Wait for login to succeed, then create workflow user in argocd
until ${TOOLING} ${CLUSTER_DIR}/argocd-login.sh; do
  sleep 1
done

until ${TOOLING} bash --login shellspec -c test -s bash ; do
  sleep 5
done

echo "🥳 All done."
