Include spec/support.sh

Describe 'k3d development cluster'

  Describe "Traefik"
    It "redirects http to https"
      When call curl $CURL_ARGS -I "http://any-service.k3d.local.with-humans.org/"
      The status should be success
      The result of "redirect_url()" should equal "https://any-service.k3d.local.with-humans.org/"
    End

    It "exposes the Traefik dashboard"
      When call curl $CURL_ARGS https://traefik-dashboard.k3d.local.with-humans.org/dashboard/
      The status should be success
      The result of "http_code()" should equal "200"
    End
  End

  Describe "Argo"
    It "exposes the argo-cd interface"
      When call curl $CURL_ARGS https://argocd.k3d.local.with-humans.org/
      The status should be success
      The result of "http_code()" should equal "200"
    End

    It "exposes the argo-rollouts dashboard"
      When call curl $CURL_ARGS https://argo-rollouts.k3d.local.with-humans.org/rollouts/
      The status should be success
      The result of "http_code()" should equal "200"
    End

    Describe "argocd"
      It "allows argo-workflows to sync apps in argocd"
         When run argocd admin settings rbac can argo-workflows sync applications --namespace argocd
         The status should be success
         The output should equal "Yes"
        End
      End

      It "allows the argocd admin to create applications"
         When run argocd admin settings rbac can admin create applications '*' --namespace argocd
         The status should be success
         The output should equal "Yes"
      End


      createArgocdGuestbook() {
        argocd app create guestbook \
          --repo https://github.com/argoproj/argocd-example-apps.git \
          --path guestbook \
          --dest-server https://kubernetes.default.svc \
          --dest-namespace default
      }

      deleteArgocdGuestbook() { argocd app delete --yes guestbook ;}
      AfterAll deleteArgocdGuestbook

      It "can create argocd applications"
         When run createArgocdGuestbook
         The status should be success
         The output should equal "application 'guestbook' created"
      End
    End


  Describe "Prometheus"
    It "exposes the web interface"
      When call curl $CURL_ARGS https://prometheus.k3d.local.with-humans.org/graph
      The status should be success
      The result of "http_code()" should equal "200"
    End

    It "exposes the alertmanager interface"
      When call curl $CURL_ARGS https://alertmanager.k3d.local.with-humans.org
      The status should be success
      The result of "http_code()" should equal "200"
    End

    It "exposes the blackbox interface"
      When call curl $CURL_ARGS https://prometheus-blackbox.k3d.local.with-humans.org/
      The status should be success
      The result of "http_code()" should equal "200"
    End

  End

  Describe "Loki"
    It "can be queried using logcli"
      When call query_loki '{app="loki"}'
      The status should be success
      The lines of stdout should not equal "0"
    End
  End

  Describe "Grafana"
    grafana_datasources() { env echo "$1" | jq -r '.[].uid' ; }

    It "exposes the web interface"
      When call curl $CURL_ARGS https://grafana.k3d.local.with-humans.org/
      The status should be success
      The result of "http_code()" should equal "302"
      The result of "redirect_url()" should equal "https://grafana.k3d.local.with-humans.org/login"
    End

    It "has Loki configured as datasource"
      When call grafana_api_call '/api/datasources'
      The status should be success
      The result of "grafana_datasources()" should include "loki"
    End

    It "can query Loki"
      When call grafana_loki_query_instant 'count_over_time({app=\"loki\"}[15m])'
      The output should satisfy formula "value > 0"
    End

    It "has Prometheus configured datasource"
      When call grafana_api_call '/api/datasources'
      The status should be success
      The result of "grafana_datasources()" should include "prometheus"
    End

    It "can query Prometheus"
      status() { jq -r '.results.A.status' ; }
      When call grafana_prometheus_query_instant 'grafana_build_info'
      The status should be success
      The result of "status()" should equal "200"
    End

    It "has its log files aggregated"
      When call query_loki '{app="grafana", namespace="observability", container="grafana"} | logfmt | level = `info`'
      The status should be success
      The lines of stdout should not equal 0
    End

  End

End
