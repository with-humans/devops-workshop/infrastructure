Include spec/support.sh


delete_rollout_and_wait() {
  ! kubectl delete -f "$(dirname ${SHELLSPEC_SPECFILE})/rollout_fixture.yaml" --ignore-not-found=false 2>/dev/null
  while ! ! kubectl get rollouts.argoproj.io rollout-spec-rollout 2>/dev/null;
  do
    sleep 0.1
  done
  echo "Rollout deleted"
}

wait_for_healthy_ingress() {
  echo "Waiting for ingress to return an OK status"
  while ! curl --max-time 1 --connect-timeout 1 -s -o /dev/null --fail https://rollout-spec.k3d.local.with-humans.org;
  do 
    sleep 0.1
  done
  echo "Ingress healthy"
}

call_api_n_times() {
  local -r TIMES=$1
  local -r URL=https://rollout-spec.k3d.local.with-humans.org
  for i in $(seq 1 $TIMES);
  do
    curl -H "Connection: close" --http1.1 $CURL_ARGS_API $URL
    sleep 0.05
  done
}

Describe 'Rollout'

  Describe "Progressively Rolling Out"
    setup() {
      delete_rollout_and_wait
      kubectl apply -f "$(dirname ${SHELLSPEC_SPECFILE})/rollout_fixture.yaml"
      kubectl wait --for=condition=Available rollout rollout-spec-rollout
      kubectl wait --for=condition=Healthy rollout rollout-spec-rollout
      wait_for_healthy_ingress
      kubectl patch rollout rollout-spec-rollout --type=json -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/value", "value": "'$(openssl rand -hex 16)'"}]'
      kubectl wait --for=condition=Available rollout rollout-spec-rollout
      kubectl wait --for=condition=Paused rollout rollout-spec-rollout
      STABLE_SET=$(kubectl get rollouts.argoproj.io rollout-spec-rollout -o template --template '{{.status.stableRS}}')
      NEXT_SET=$(kubectl get rollouts.argoproj.io rollout-spec-rollout -o template --template '{{.status.currentPodHash}}')
    }

    cleanup() {
      delete_rollout_and_wait
    }
    BeforeEach 'setup'
    AfterEach 'cleanup'

    It "forwards traffic only to the stable version at the start"
      When run call_api_n_times 50
      The output should include "${STABLE_SET}"
      The output should not include "${NEXT_SET}"
    End

    It "splits traffic between both sets on the second pause"
      kubectl argo rollouts promote rollout-spec-rollout
      kubectl wait --for=condition=Progressing rollout rollout-spec-rollout
      kubectl wait --for=condition=Paused rollout rollout-spec-rollout
      wait_for_healthy_ingress
      kubectl wait --for=condition=Ready pod -l "rollouts-pod-template-hash=${STABLE_SET}"
      kubectl wait --for=condition=Ready pod -l "rollouts-pod-template-hash=${NEXT_SET}"
      
      When run call_api_n_times 50
      The output should include "${STABLE_SET}"
      The output should include "${NEXT_SET}"
    End

    It "only sends traffic to the new version on the third pause"
      kubectl argo rollouts promote --full rollout-spec-rollout
      kubectl wait --for=condition=Completed rollout rollout-spec-rollout
      kubectl wait --for=condition=Healthy rollout rollout-spec-rollout
      wait_for_healthy_ingress
      kubectl wait --for=condition=Ready pod -l "rollouts-pod-template-hash=${NEXT_SET}"

      When run call_api_n_times 50
      The output should not include "${STABLE_SET}"
      The output should include "${NEXT_SET}"
    End
  End

End
