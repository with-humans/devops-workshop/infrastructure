apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "git-server.fullname" . }}-statefulset
spec:
  selector:
    matchLabels:
      {{- include "git-server.selectorLabels" . | nindent 6 }}
  serviceName: {{ include "git-server.fullname" . }}
  replicas: 1
  template:
    metadata:
      labels:
        {{- include "git-server.labels" . | nindent 8 }}
    spec:
      containers:
      - name: git-server
        image: alpine:latest
        ports:
          - containerPort: 80
            name: http
            protocol: TCP
        command: ["/bin/sh", "-c"]
        args:
          - |
            set -exu
            trap exit TERM
            chmod 777 /dev/fd/2

            apk add --no-cache git git-daemon lighttpd lighttpd-mod_auth curl
            mkdir -p /git
            
            git clone --bare --shared "{{ required "needs upstream" .Values.gitUpstream }}" /git
            touch /git/hooks/post-receive
            chmod a+x /git/hooks/post-receive
            cat <<\EOF > /git/hooks/post-receive
            #!/bin/sh
            set -xeu

            run() {
              env
              echo $@
              while read oldrev newrev ref; do
                {{ if .Values.postReceiveWebhook }}
                  set +e
                  curl \
                    -H "X-GitHub-Event: push" \
                    -H "X-Gogs-Event: push" \
                    -H "X-Gitea-Event: push" \
                    --silent \
                    --verbose \
                    "{{ .Values.postReceiveWebhook }}" \
                    -d '{"ref": "'$ref'", "before": "'$oldrev'", "after": "'$newrev'"}'
                  set -e
                {{ end }}
                echo "$oldrev $newrev $ref"
              done
            }

            run $@
            EOF

            git --git-dir /git/ config http.receivepack true
            
            chown -R lighttpd:lighttpd /git

            cat <<\EOF > /etc/lighttpd/lighttpd.conf
            var.basedir  = "/var/www/localhost"
            var.logdir   = "/var/log/lighttpd"
            var.statedir = "/var/lib/lighttpd"

            server.modules = (
                "mod_auth",
                "mod_access",
                "mod_accesslog",
                "mod_cgi",
                "mod_alias",
                "mod_setenv"
            )

            include "mime-types.conf"

            server.username      = "lighttpd"
            server.groupname     = "lighttpd"

            server.document-root = var.basedir + "/htdocs"
            server.pid-file      = "/run/lighttpd.pid"

            accesslog.filename   = "/dev/fd/2"


            alias.url += ( "/git" => "/usr/libexec/git-core/git-http-backend" )
            $HTTP["url"] =~ "^/git" {
              cgi.assign = ("" => "")
              setenv.add-environment = (
                "GIT_PROJECT_ROOT" => "/git",
                "GIT_HTTP_EXPORT_ALL" => "true"
              )
            }
            EOF
            
            exec lighttpd -D -f /etc/lighttpd/lighttpd.conf
