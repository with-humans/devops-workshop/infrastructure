#!/bin/bash
set -euxo pipefail
CLUSTER_DIR=$(dirname "$0")
source "${CLUSTER_DIR}/_config.sh"

# helm apply requires the helm diff plugin to be installed
(cd ${LOCAL_CLUSTER_DIR} && ${TOOLING} sh -c 'helm plugin list | grep "^diff\s" || (echo "Install helm-diff: helm plugin install https://github.com/databus23/helm-diff" && exit 1)')

(cd ${LOCAL_CLUSTER_DIR} && ${TOOLING} helmfile $@)
