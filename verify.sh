#!/bin/bash
set -euxo pipefail
source _config.sh
${TOOLING} ${CLUSTER_DIR}/argocd-login.sh
${TOOLING} bash --login shellspec -c test -s bash $@
